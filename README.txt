Domain Switch for Drupal 8

Introduction:
This modules allow the users to switch between multiple domains. The module needs to be installed and configured on all the sites on which the switch user operation is to be done.

Permissions:
The users with the permission "Domain Switch" permission will be able to make the switch.

Configurations:
Navigate to configuration >> Domain Switch and fill in the form as per the guidlines mentioned in the form.

  -- Fill in the Secret key, this key should be same for all the sites on which you want the switch functionality to work.
  -- Fill in the URL in the field "Switch To" to allow the module to switch to the listed domain.
  -- Fill in the domains which you want to allow to switch from to the current site.

Working: To make a switch just navigate to the menu Domains in the admin menu, and you will see the list of domains that are listed in the "Switch To" field in the Domain switch config form.
