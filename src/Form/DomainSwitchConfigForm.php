<?php

namespace Drupal\domain_switch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure for the domain_switch module.
 */
class DomainSwitchConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'DomainSwitcherConfigForm_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'domain_switch.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('domain_switch.settings');

    $switch_to = $config->get('switch_to');
    foreach ($switch_to as $key => $value) {
      $output = $value . "|" . $key;
      $domain_name[] = $output;
    }
    $switch_to_val = implode(PHP_EOL, $domain_name);
    $switch_from = $config->get('switch_from');
    $switch_from_val = implode(PHP_EOL, $switch_from);

    $form['hash_salt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $config->get('hash_salt'),
      '#description' => 'Please enter the 16 digit hash key. Use the same key for all the domains where you want to allow the user to switch to and switch from.',
      '#maxlength' => 16,
    ];

    $form['switch_to'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Switch to domains'),
      '#default_value' => $switch_to_val,
      '#description' => ' Enter the domains to which you want to switch.<br>Enter one domain per line, in the format Domain URL|Domain Name. e.g. https://www.google.com|Google<br>Leave it blank if you do not want to switch to any domain.<br>',

    ];
    $form['switch_from'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Switch from domains'),
      '#default_value' => $switch_from_val,
      '#description' => ' Enter the domains which you want to allow to switch from.<br>
         Enter one domain per line. e.g. https://www.google.co.in<br>
         Leave it blank if you do not want to allow to switch from any other domain to switch to your domain.<br>
         To allow the domain https://www.firstsite.com to switch to the domain https://www.secondwebsite.com. Then add https://www.firstsite.com to this field on https://www.secondwebsite.com<br>',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $hash_salt = $form_state->getValue('hash_salt');
    if (strlen($hash_salt) != 16) {
      $form_state->setErrorByName('hash_salt', $this->t('The Hash key should contain 16 characters'));
    }

    $switch_to = $form_state->getValue('switch_to');
    $switch_to_exp = explode(PHP_EOL, $switch_to);

    if (count($switch_to_exp) !== count(array_unique($switch_to_exp))) {
      $form_state->setErrorByName('switch_to', $this->t('No duplicate values are allowed'));
    }

    foreach ($switch_to_exp as $string) {
      $domain = explode('|', $string);
      // Variable to check.
      $url = $domain[0];
      if ($url) {
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) {
          $form_state->setErrorByName('switch_to', $this->t('@url, is not a valid URL', ['@url' => $url]));
        }
      }
    }

    $switch_from = $form_state->getValue('switch_from');
    $switch_from_exp = explode(PHP_EOL, $switch_from);

    if (count($switch_from_exp) !== count(array_unique($switch_from_exp))) {
      $form_state->setErrorByName('switch_from', $this->t('@url, is not a valid URL', ['@url' => $url]));
    }

    foreach ($switch_from_exp as $key => $value) {
      $url = $value;
      // Validate url.
      if ($url) {
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) {
          $form_state->setErrorByName('switch_from', $this->t('@url, is not a valid URL', ['@url' => $url]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $switch_to = $form_state->getValue('switch_to');
    $switch_from = $form_state->getValue('switch_from');
    $switch_to_exp = explode(PHP_EOL, $switch_to);
    $switch_from_exp = explode(PHP_EOL, $switch_from);
    if (!empty($switch_to)) {
      foreach ($switch_to_exp as $string) {
        $domain = explode('|', $string);
  	    $domain_key = preg_replace("/[^ \w]+/", "", $domain[1]);
        $switch_to_values[$domain_key] = trim($domain[0], " ");
      }
    }

    foreach ($switch_from_exp as $key => $value) {
      if (!empty($switch_from_exp[$key])) {
        $switch_from_values[] = trim($value, " ");
      }
    }

    // Retrieve the configuration.
    $this->configFactory->getEditable('domain_switch.settings')
    // Set the submitted configuration setting.
      ->set('hash_salt', $form_state->getValue('hash_salt'))
      ->set('switch_to', $switch_to_values)
      ->set('switch_from', $switch_from_values)
      ->save();

    parent::submitForm($form, $form_state);
	\Drupal::cache('menu')->invalidateAll();
	\Drupal::service('plugin.manager.menu.link')->rebuild();
  }

}
