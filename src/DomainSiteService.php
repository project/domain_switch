<?php

namespace Drupal\domain_switch;

/**
 * Return Domains lists.
 */
class DomainSiteService {

  /**
   * Gets the domains from the configurations.
   */
  public function getDomains() {
    $config = \Drupal::config('domain_switch.settings');
    $switch_to = $config->get('switch_to');
    foreach ($switch_to as $key => $value) {
      $domain = str_replace('_', '.', $key);
      $sites[$domain] = $value;
    }
    return $sites;
  }

}
