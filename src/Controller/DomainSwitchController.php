<?php

namespace Drupal\domain_switch\Controller;

/**
 * @file
 * Contains \Drupal\domain_switch\Controller\DomainSwitchController.
 */

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
// use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
// use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\domain_switch\DomainSiteService;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Performs the switch operation between the domains.
 */
class DomainSwitchController extends ControllerBase {
  /**
   * Gets the user details.
   *
   * @var account\Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The Domain Site service.
   *
   * @var \Drupal\domain_switch\DomainSiteService
   */
  protected $domainSiteService;

  /**
   * Constructor.
   *
   * @param Drupal\Core\Session\AccountProxy $account
   *   Gets the user details.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Checks if the domain_switch module exists.
   * @param Drupal\domain_switch\DomainSiteService $domainSiteService
   *   Gets the list of domains.
   */
  public function __construct(AccountProxy $account, ModuleHandlerInterface $module_handler, DomainSiteService $domainSiteService) {
    $this->account = $account;
    $this->moduleHandler = $module_handler;
    $this->domainSiteService = $domainSiteService;
  }

  /**
   * Function to create the container interface.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Creates the container interface.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('domain_switch.sites')
    );
  }

  /**
   * Get encrypted string.
   *
   * @param string $string
   *   Gets the encrypted string.
   * @param string $salt
   *   Gets the hash key.
   *
   * @return string
   *   encrypted string
   */
  public function encryptString($string, $salt) {
    //return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $string, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    $cipher_method = 'AES-128-CTR';
    $enc_key = openssl_digest($salt, 'SHA256', TRUE);
    $enc_iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($cipher_method));
    $crypted_token = openssl_encrypt($string, $cipher_method, $enc_key, 0, $enc_iv) . "::" . bin2hex($enc_iv);
    unset($token, $cipher_method, $enc_key, $enc_iv);
    return $crypted_token;
  }

  /**
   * Get decrypted string.
   *
   * @param string $string
   *   Gets the encrypted string.
   * @param string $salt
   *   Gets the has key.
   *
   * @return string
   *   decrypted string
   */
  public function decryptString($string, $salt) {
    //return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($string), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    list($string, $enc_iv) = explode("::", $string);;
    $cipher_method = 'AES-128-CTR';
    $enc_key = openssl_digest($salt, 'SHA256', TRUE);
    $token = openssl_decrypt($string, $cipher_method, $enc_key, 0, hex2bin($enc_iv));
    unset($crypted_token, $cipher_method, $enc_key, $enc_iv);
    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function settings() {
    $config = $this->config('domain_switch.settings');
    $switch_to = $config->get('switch_to');
    $switch_from = $config->get('switch_from');
    $hash_salt = $config->get('hash_salt');
    // $user_roles = $this->account->getRoles();
    $domain = \Drupal::request()->query->get('domain');
    if (!empty($domain) && (\Drupal::currentUser()->hasPermission('switch permission'))) {
      $email = $this->account->getEmail();
      $curr_time = time();
      $url = $domain . '/admin/domain/access';
      $user_email = $this->encryptString($email, $hash_salt);
      $time = $this->encryptString($curr_time, $hash_salt);
      $option = [
        'query' => ['user_email' => $user_email, 'token' => $time],
        'absolute' => TRUE,
      ];
      $path = Url::fromUri($url, $option)->toString();
      $response = new TrustedRedirectResponse($path, 302);
      $response->send();
      //user_logout();
      return;
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    $config = $this->config('domain_switch.settings');
    $hash_salt = $config->get('hash_salt');
	  $time_limit = $config->get('time_token');
    $email = str_replace(' ', '+', \Drupal::request()->query->get('user_email'));
    $token = str_replace(' ', '+', \Drupal::request()->query->get('token'));

    $user_email = $this->decryptString($email, $hash_salt);
    $time = $this->decryptString($token, $hash_salt);
    $curr_time = time();
    if (!empty($user_email) && !empty($time) && $this->moduleHandler->moduleExists('domain_switch')) {
      if ($curr_time >= $time && ($curr_time - $time < $time_limit)) {
        $user = user_load_by_mail($user_email);
        if ($user->hasPermission('switch permission')) {
          user_login_finalize($user);
          $message = "User switched from " . $_SERVER['HTTP_REFERER'];
          \Drupal::logger('domain_switch')->notice($message);
          return $this->redirect('user.page');
        }
      }
      else {
        throw new AccessDeniedHttpException();
      }
    }
    else {
      throw new AccessDeniedHttpException();
    }
  }
}
