<?php

namespace Drupal\domain_switch\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\domain_switch\DomainSiteService;

/**
 * Derivative class that provides the menu links for the Products.
 */
class SiteMenuLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Gets the entity manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Domain Site service.
   *
   * @var \Drupal\domain_switch\DomainSiteService
   */
  protected $domainSiteService;

  /**
   * Creates a SiteMenuLink instance.
   *
   * @param string $base_plugin_id
   *   The base plugin id.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param Drupal\domain_switch\DomainSiteService $domainSiteService
   *   Gets the domain list using the custom service.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager, DomainSiteService $domainSiteService) {
    $this->entityTypeManager = $entity_type_manager;
    $this->domainSiteService = $domainSiteService;
  }

  /**
   * Creates the container interface.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Creates the container interface.
   * @param string $base_plugin_id
   *   Creates the base plugin id.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('domain_switch.sites')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $sites = $this->domainSiteService->getDomains();

    foreach ($sites as $key => $site) {
      $links['sites' . $key] = [
        'title' => $key,
        'route_name' => 'domain_switch.settings',
        'route_parameters' => [
          'domain' => $site,
        ],
      ] + $base_plugin_definition;
    }

    return $links;
  }

}
