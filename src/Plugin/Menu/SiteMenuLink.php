<?php

namespace Drupal\domain_switch\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 * @file
 * Contains \Drupal\domain_switch\Plugin\Menu\SiteMenuLink.
 */

/**
 * Represents a menu link for a single Domain.
 */
class SiteMenuLink extends MenuLinkDefault {}
